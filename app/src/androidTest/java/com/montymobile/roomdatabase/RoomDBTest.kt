package com.montymobile.roomdatabase

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RoomDBTest:TestCase()
{
    private lateinit var roomDB: RoomDB
    private lateinit var userDAO: DatabaseDAO

    @Before
    public override fun setUp() {
        super.setUp()

        roomDB = Room.inMemoryDatabaseBuilder(ApplicationProvider.getApplicationContext(),RoomDB::class.java).build()
        userDAO = roomDB.getFavouritesDAO()
    }

    @After
    fun closeDB()
    {
        roomDB.close()
    }

    @Test
    fun writeAndReadSpend()= runBlocking()
    {
        var user = FavouritesBO(0,"faraz","")
        userDAO.insertFavouriteDrink(user)
        var res=true
        userDAO.getAllFavourites().value?.forEach {
            res=it.name.equals(user.name)
        }
        assertThat(res).isEqualTo(true)
    }
}