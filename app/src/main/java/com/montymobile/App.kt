package com.montymobile.callsignature

import android.app.Application
import android.content.Context
import com.montymobile.callsignature.networking.buildApiService
import com.montymobile.videorbt.utils.SharedPreferenceUtils

class App: Application()
{
    companion object {

        private lateinit var instance: App
        // lateinit var appDefaultScope: CoroutineScope

        val apiService by lazy {
            buildApiService()
        }

        fun getContext(): Context = instance

        fun getUserPreferences(): SharedPreferenceUtils = SharedPreferenceUtils(instance)

    }


    override fun onCreate() {
        super.onCreate()
        instance = this
//        LocaleUtils.initialize(this, LocaleUtils.getAppLanguage());
//        LocaleUtils.setLocale(this, LocaleUtils.getAppLanguage());
        // appDefaultScope = CoroutineScope(Dispatchers.Default + applicationJob)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
    }
}