package com.montymobile

class Constants
{
    companion object {
        const val DATABASE_NAME = "FAVOURITES_DB"
        var DATE_FORMATE_OLD = "MMM/dd/yyyy hh:mm:ss"
        var TIME_FORMAT_12 = "hh:mm a"
    }
}