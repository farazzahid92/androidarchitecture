package com.montymobile.interfaces

import com.montymobile.callsignature.networking.ApiEndPoints
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiInterface
{
    @POST(ApiEndPoints.SEARCH_BY_NAME)
    suspend fun searchByName(@Query("s") name:String):Any
}