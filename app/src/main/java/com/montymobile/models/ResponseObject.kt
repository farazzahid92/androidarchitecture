package com.montymobile.models

data class ResponseObject<T>(
    val action: Int,
    val message: String?,
    val developerMessage: String,
    val data: T,
    val result: T
)