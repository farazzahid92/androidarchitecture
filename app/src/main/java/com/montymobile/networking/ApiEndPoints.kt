package com.montymobile.callsignature.networking

object ApiEndPoints
{
    const val SEARCH_BY_NAME = "api/json/v1/1/search.php?s"
    const val SEARCH_BY_ALPHABET = "api/json/v1/1/search.php?f"
}